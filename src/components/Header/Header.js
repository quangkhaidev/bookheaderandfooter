"use client";
import useWindowSize from "@/hooks/useWindowsize";
import React, { useEffect, useState } from "react";
import HeaderMobile from "./HeaderMobile";
import HeaderDesktop from "./HeaderDesktop";

export default function Header() {
  const [isMobile, setIsMobile] = useState(false);
  const { widthWindow, heightWindow } = useWindowSize();
  useEffect(() => {
    if (widthWindow <= 1023.98) {
      console.log("window.innerWidth: ", widthWindow);
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  });
  return isMobile ? <HeaderMobile /> : <HeaderDesktop />;
}
