import React from "react";
import TopBannerBlock from "./TopBannerBlock/TopBannerBlock";
import MenuDesktop from "./Menu/MenuDesktop";

export default function HeaderDesktop() {
  return (
    <header>
      {/* top-banner-block */}
      <TopBannerBlock />
      {/* menu */}
      <MenuDesktop />
    </header>
  );
}
