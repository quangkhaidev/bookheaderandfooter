import React from "react";
import "../../../../css/topBannerBlock.css";
import Image from "next/image";

export default function TopBannerBlock() {
  return (
    <div className="topBannerBlock">
      <div className="container">
        <a href="#">
          <img
            src="https://cdn0.fahasa.com/media/wysiwyg/Thang-08-2023/Fahasa_sinhnhat_banner_Header_1263x60.jpg"
            alt="topBannerBlock"
          />
        </a>
      </div>
    </div>
  );
}
