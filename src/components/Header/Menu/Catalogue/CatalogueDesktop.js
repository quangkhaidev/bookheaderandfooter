import React from "react";
import iconMenu from "../../../../../public/ico_menu.svg";
import iconArrowDown from "../../../../../public/icon_seemore_gray.svg";
import Image from "next/image";
import "../../../../../css/catalogueDesktop.css";
export default function CatalogueDesktop() {
  return (
    <div className="catalogueDesktop">
      {/* Icon */}
      <Image src={iconMenu} alt="Icon Catalogue" />
      <Image src={iconArrowDown} alt="Icon iconArrowDown" />
      {/* DropDown menu */}
    </div>
  );
}
