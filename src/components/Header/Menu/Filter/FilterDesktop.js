import React from "react";
import "../../../../../css/filterDesktop.css";
import Image from "next/image";
import logoSearch from "../../../../../public/ico_search_white.svg";

export default function FilterDesktop() {
  let submitForm = () => {
    console.log("first");
  };
  return (
    <div className="filterDesktop" onSubmit={submitForm()}>
      <form className="filterForm ">
        <input placeholder="Tìm kiếm sản phẩm mong muốn" className="grow" />
        <button type="submit">
          <Image src={logoSearch} alt="Search" />
        </button>
      </form>
    </div>
  );
}
