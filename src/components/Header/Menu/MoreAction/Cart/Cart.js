import Image from "next/image";
import React from "react";
import cartIcon from "../../../../../../public/ico_cart_gray.svg";

export default function Cart() {
  return (
    <div className="cart item">
      {/* img */}
      <div className="icon">
        <Image src={cartIcon} alt="icon" />
      </div>
      <p className="text">Giỏ hàng</p>
    </div>
  );
}
