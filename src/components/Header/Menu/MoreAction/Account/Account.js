import Image from "next/image";
import React from "react";
import accountIcon from "../../../../../../public/ico_account_gray.svg";
export default function Account() {
  return (
    <div className="account item">
      {/* img */}
      <div className="icon">
        <Image src={accountIcon} alt="icon" />
      </div>
      <p className="text">Tài khoản</p>
    </div>
  );
}
