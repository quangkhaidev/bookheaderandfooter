import React from "react";
import bellIcon from "../../../../../../public/ico_noti_gray.svg";
import Image from "next/image";

export default function Notification() {
  return (
    <div className="notification item">
      {/* img */}
      <div className="icon">
        <Image src={bellIcon} alt="icon" />
      </div>
      {/* text */}
      <p className="text">Thông báo</p>
    </div>
  );
}
