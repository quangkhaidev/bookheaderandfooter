import Image from "next/image";
import React from "react";
import Logo from "../../../../public/logo.png";
import CatalogueDesktop from "./Catalogue/CatalogueDesktop";
import "../../../../css/MenuDesktop.css";
import FilterDesktop from "./Filter/FilterDesktop";
import MoreAction from "./MoreAction/MoreAction";

export default function MenuDesktop() {
  return (
    <nav id="menuDesktop">
      <div className="container">
        {/* Logo */}
        <div className="logo" style={{ maxWidth: "216.36px" }}>
          <Image src={Logo} alt="Logo" />
        </div>
        {/* Catalogue */}
        <CatalogueDesktop />
        {/* Filter */}
        <FilterDesktop />
        {/* More Actions */}
        <MoreAction />
      </div>
    </nav>
  );
  s;
}
